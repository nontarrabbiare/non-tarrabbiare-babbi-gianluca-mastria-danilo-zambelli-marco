package model;

import java.io.Serializable;

/**
 * Color is the enumeration for the stand of token and cell 
 *
 */
public enum Standing implements Serializable{

	AT_HOME, PLAYING, ARRIVED
}
